const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const generators = require('../src/generators');

router.get('/', function(req, res, next) {
    res.render('index', {
        generators: generators,
        baseUrl: req.protocol + '://' + req.get('host'),
    });
});

module.exports = router;
