const Generator = require('../../abstractGenerator');
const {randomElement, randomise} = require('../../helpers');
const sets = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class Password extends Generator {

    params() {
        return {
            length: {
                type: 'int',
                min: 3,
                max: 128,
                default: 12,
            },
            lower: {
                type: 'bool',
            },
            upper: {
                type: 'bool',
            },
            digits: {
                type: 'bool',
            },
            symbols: {
                type: 'bool',
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        let chars = '';
        for (let set in sets) {
            if (sets.hasOwnProperty(set)) {
                if (params[set]) {
                    chars += sets[set];
                }
            }
        }

        if (chars.length === 0) {
            chars = Object.values(sets).join('');
        }

        return randomise(chars, params.length);
    }

    validate(value) {
        return null;
    }
};
