module.exports = {
    '_': {
        'ean': new (require('./generators/_/ean')),
        'iban': new (require('./generators/_/iban')),
        'isbn': new (require('./generators/_/isbn')),
        'password': new (require('./generators/_/password')),
    },
    'DE': {
        'tin': new (require('./generators/DE/tin')),
    },
    'PL': {
        'id': new (require('./generators/PL/id')),
        'nip': new (require('./generators/PL/nip')),
        'pesel': new (require('./generators/PL/pesel')),
        'regon': new (require('./generators/PL/regon')),
    },
    'NL': {
        'bsn': new (require('./generators/NL/bsn')),
    },
    'GB': {
        'utr': new (require('./generators/GB/utr')),
        'nino': new (require('./generators/GB/nino')),
    },
    'ES': {
        'nif': new (require('./generators/ES/nif')),
    },
    'IT': {
        'cf': new (require('./generators/IT/cf')),
    },
};
