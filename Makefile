install:
	yarn

start:
	node_modules/.bin/avris-daemonise start assets node_modules/.bin/encore dev-server
	sleep 10
	node_modules/.bin/avris-daemonise start webserver bin/www

stop:
	node_modules/.bin/avris-daemonise stop assets
	node_modules/.bin/avris-daemonise stop webserver

deploy: install
	yarn build
	echo "google.com, pub-5185551150939482, DIRECT, f08c47fec0942fa0" > public/ads.txt
	sudo supervisorctl restart generator
